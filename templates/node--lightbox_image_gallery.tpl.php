<?php
foreach($node->lightboximage_field['und'] as $value){	  
    $image['uri']= $value['uri'];
    $image['title'] = $value['title'];
	$images[]=$image;
 }
?>

<!-- The container for the lightbox images -->

<?php
 foreach($images as $image){?>
		<div class="galleryimage_small">
          <img src="<?php print file_create_url($image['uri']);?>" class="img-responsive">
		  <?php if($image['title']):?>
		  <div class="image_title"><?php print $image['title'];?> </div>
		  <?php endif;?>
		</div>	
<?php
}
?>

<!-- The container for the popup image -->

<div class="galleryimage_big display">
	  <div class="image_open_main"> 
		  <div class="popup"> 
			 <div class="gallery_img">
				<div class="header_pop"> 
				   <button type="button" class="close" class="fa fa-times">Close</button>
					<div class="prev">prev</div>
					<div class="next">next</div>
				 </div>
				 <div id="title"></div>
				 <img class="slider" src=''>
			  </div>
			<div class="overlay_gallery"></div>
		</div>
	</div>
</div>