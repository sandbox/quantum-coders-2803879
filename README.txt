
LIGHTBOX IMAGE GALLERY MODULE
-----------------------------

Created by Quantum Coders LLC


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation & Usage
 * Overriding Templates
 * Credits
 

INTRODUCTION
------------

Lightbox Image Gallery module that creates appealing photo galleries from content type.

Features:
    - The Contents type is already created by the module.
    - Displays images in a responsive grid format. 
    - Provides different options for controlling image gallery module.
    - Easy to theme.
    - Contains a sample gallery feature that would save your initial efforts.
 
 
INSTALLATION & USAGE
--------------------

1. Download module, extract and copy "Lightbox Image Gallery" directory to your sites/SITENAME/modules directory.

2. Enable the module from the Admin > Modules.

3. This will create a content type in system which will be used to create multiple galleries in system.


OVERRIDING TEMPLATES
--------------------

Image Gallery output can be controlled easily by overriding the template files included in the "themes" folder. Which exists in the same module directory. The theme folder contains the following files:

1. node--lightbox_image_gallery.tpl: A normal template file contains the HTML for the gallery grid.

To override the file, simply copy to your default theme directory and modify HTML as desired.


CREDITS
-------

This module is bundled with couple of Javascript libraries that were developed by:

jQuery JavaScript Library v1.11.1